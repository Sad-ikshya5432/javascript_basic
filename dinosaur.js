const dinosaur=function()
{
    this.parent=null;
    this.height=150;
    this.width=150;
    this.x=0;
    this.y=350;
    this.velocity=5;
    this.init=function(parent)
    {
        this.parent=parent;

        this.dinosaurDOM=document.createElement("img");
        this.dinosaurDOM.src="image/dinosaur.jpg";
        this.dinosaurDOM.style.height=this.height+'px';
        this.dinosaurDOM.style.width=this.width+'px';
        this.dinosaurDOM.style.position="absolute";
        this.dinosaurDOM.style.left=this.x+'px';
        this.dinosaurDOM.style.top=this.y+'px';


        this.parent.containerDOM.appendChild(this.dinosaurDOM);
        return this;
    }
    this.update=function()
    {
        console.log("update");
        this.dinosaurDOM.style.left=this.x+'px';
        this.dinosaurDOM.style.top=this.y+'px';

    }
    this.moveFunction=function(event)
    {
        console.log(event);
        console.log(this);
        if(event.key==='w')
        {
            if(this.y<0)
            {
                this.y=0;
            }
            else
            this.y-=this.velocity;
        }
        else if(event.key==='s')
        {
            if(this.y>(this.parent.height-this.height))
            {
                this.y=(this.parent.height-this.height);
            }
            else
                this.y+=this.velocity;
            
        }
        else if(event.key==='a')
        {
            if(this.x<0)
            {
                this.x=0;
            }
            else
                this.x-=this.velocity;
            
        }
        else if(event.key==='d')
        {
            if(this.x>(this.parent.width-this.width))
            {
                this.x=(this.parent.width-this.width);
            }
            else
            this.x+=this.velocity;
        }
    }

}

const container=function()
{
    this.containerDOM=document.getElementById("container");
    this.height=500;
    this.width=500;
    this.dinosaurobj=null;
    this.init=function()
    {
        console.log("constructor");
        this.containerDOM.style.height=this.height+'px';
        this.containerDOM.style.width=this.width+'px';
        this.containerDOM.style.border="1px solid black";
        this.containerDOM.style.position="relative";

 
        
        this.dinosaurobj=new dinosaur().init(this);
        document.onkeypress=(event)=>{this.dinosaurobj.moveFunction(event)};
        return this;
    }
    this.update=function()
    {
        this.dinosaurobj.update();
    }
}
let obj1=new container().init();
setInterval(obj1.update.bind(obj1),100);




